# sgforient

Rotates pre-made moves (AB and AW properties in SGF) to a prefered orientation. Best used with sgfstrip and other sgf tools when creating puzzles. Can be extended to also rotate all of the moves (e.g. for solutions). The heuristic determining the current orientation focuses on counting stones in specific quadrants, so this operation may not be meaningful for larger or full board positions.

## Usage
sgforient [-t|-h] \<fileName\>

Spits the rotated sgf to standard output.

**-t (--targetOrientation)** takes one of {top, left, bottom, right} to select the preferred orientation (top being the default).
