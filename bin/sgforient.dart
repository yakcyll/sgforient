import 'dart:convert';
import 'dart:io';

import 'package:args/args.dart';
import 'package:sgfparser/sgf/sgfParser.dart';
import 'package:sgfparser/sgf/sgfWriter.dart';
import 'package:sgfparser/sgf/treeNode.dart';
import 'package:sgfparser/utils/pair.dart';

enum Orientation {Top, Left, Bottom, Right}
Map<String, Orientation> stringToOrientation = {
  'top': Orientation.Top,
  'left': Orientation.Left,
  'bottom': Orientation.Bottom,
  'right': Orientation.Right,
};

Pair<int, int> convertStoneToCoord(String stone) {
  String actualStone = stone.toLowerCase();
  return Pair<int, int>(
    actualStone.codeUnitAt(0) - 'a'.codeUnitAt(0),
    actualStone.codeUnitAt(1) - 'a'.codeUnitAt(0)
  );
}

String convertCoordToStone(Pair<int, int> coord) {
  return String.fromCharCodes([coord.first + 'a'.codeUnitAt(0), coord.second + 'a'.codeUnitAt(0)]);
}

bool isCoordAboveTopLeftBottomRightDiagonal(Pair<int, int> coord) {
  return coord.first >= coord.second;
}

bool isCoordAboveBottomLeftTopRightDiagonal(Pair<int, int> coord, int boardWidth) {
  return boardWidth - coord.first - 1 >= coord.second;
}

Map<Orientation, int> countStonesPerQuadrant(List<String> stones, int boardWidth) {
  Map<Orientation, int> result = {
    for (Orientation orientation in Orientation.values)
      orientation : 0
  };

  Map<Pair<bool, bool>, Orientation> diagonalPositionToOrientation = {
    Pair<bool, bool>(true, true): Orientation.Top,
    Pair<bool, bool>(false, true): Orientation.Left,
    Pair<bool, bool>(false, false): Orientation.Bottom,
    Pair<bool, bool>(true, false): Orientation.Right
  };

  for (String stone in stones) {
    Pair<int, int> coord = convertStoneToCoord(stone);

    Pair<bool, bool> positionRelativeToDiagonals = Pair<bool, bool>(
      isCoordAboveTopLeftBottomRightDiagonal(coord),
      isCoordAboveBottomLeftTopRightDiagonal(coord, boardWidth)
    );

    result[diagonalPositionToOrientation[positionRelativeToDiagonals]] += 1;
  }

  return result;
}

Orientation findOrientationOfPosition(SGFTreeNode sgfTree) {
  int boardWidth = 19;
  if (sgfTree.sequence[0].properties.containsKey('SZ')) {
    boardWidth = int.parse(sgfTree.sequence[0].properties['SZ'][0]);
  }

  List<String> blackStones = sgfTree.sequence[0].properties['AB'];
  List<String> whiteStones = sgfTree.sequence[0].properties['AW'];

  Map<Orientation, int> blackStonesPerQuadrant = countStonesPerQuadrant(blackStones, boardWidth);
  Map<Orientation, int> whiteStonesPerQuadrant = countStonesPerQuadrant(whiteStones, boardWidth);


  List<Pair<Orientation, int>> listOfBlackQuadrants = blackStonesPerQuadrant.entries.map(
    (quadrant) => Pair(quadrant.key, quadrant.value)).toList();
  listOfBlackQuadrants.sort((a, b) => a.second.compareTo(b.second));

  List<Pair<Orientation, int>> listOfWhiteQuadrants = whiteStonesPerQuadrant.entries.map(
    (quadrant) => Pair(quadrant.key, quadrant.value)).toList();
  listOfWhiteQuadrants.sort((a, b) => a.second.compareTo(b.second));

  // This is a heuristic for the preferred rotation; I can't wrap my head
  // around a case where the diagonals are most heavily occupied and whether
  // this could change the preferred rotation.
  Orientation mostPopulatedQuadrant = (
    listOfBlackQuadrants.last.second > listOfWhiteQuadrants.last.second
    ? listOfBlackQuadrants.last
    : listOfWhiteQuadrants.last
  ).first;

  return mostPopulatedQuadrant;
}

String rotateCoordCounterClockwise(String stone, int boardWidth, {int rotationSteps = 1}) {
  Pair<int, int> rotatedCoord = convertStoneToCoord(stone);
  for (int i = 0; i < rotationSteps; ++i) {
    rotatedCoord = Pair<int, int>(rotatedCoord.second, boardWidth - rotatedCoord.first - 1);
  }
  return convertCoordToStone(rotatedCoord);
}

void rotateTree(SGFTreeNode sgfTree, int rotationSteps) {
  int boardWidth = 19;
  if (sgfTree.sequence[0].properties.containsKey('SZ')) {
    boardWidth = int.parse(sgfTree.sequence[0].properties['SZ'][0]);
  }

  for (int i = 0; i < sgfTree.sequence[0].properties['AB'].length; ++i) {
    sgfTree.sequence[0].properties['AB'][i] = rotateCoordCounterClockwise(
      sgfTree.sequence[0].properties['AB'][i],
      boardWidth,
      rotationSteps: rotationSteps);
  }

  for (int i = 0; i < sgfTree.sequence[0].properties['AW'].length; ++i) {
    sgfTree.sequence[0].properties['AW'][i] = rotateCoordCounterClockwise(
      sgfTree.sequence[0].properties['AW'][i],
      boardWidth,
      rotationSteps: rotationSteps);
  }
}

String readSgfFile(String fileName) {
  try {
    return File(fileName).readAsStringSync(encoding: utf8);
  } on FileSystemException {
    try {
      return File(fileName).readAsStringSync(encoding: latin1);
    } on FileSystemException {
        return File(fileName).readAsStringSync(encoding: ascii);
    }
  }
}

String readSgfFromStdin() {
  List<int> chars = [];
  do {
    chars.add(stdin.readByteSync());
  } while (chars.last > 0);
  return String.fromCharCodes(chars.sublist(0, chars.length - 1));
}

void printHelp(ArgParser parser) {
  print('sgforient 1.33.7');
  print('Utility to rotate pre-made moves in an SGF to a preferred orientation.');
  print('Works with AB and AW properties.');
  print('Usage: sgforient [-t|-h] <fileName>');
  print(parser.usage);
}

void main(List<String> arguments) {
  ArgParser parser = ArgParser();

  parser.addOption('targetOrientation',
                   abbr: 't',
                   help: 'Selects which orientation should the SGF be rotated towards.',
                   allowed: ['top', 'left', 'bottom', 'right'],
                   defaultsTo: 'top');

  parser.addFlag('help',
                 abbr: 'h',
                 help: 'Displays this summary and exits.',
                 negatable: false,
                 callback: (help) { if (help) { printHelp(parser); exit(0); } });

  ArgResults results = parser.parse(arguments);

  String sgf = '';

  if (arguments.isNotEmpty) {
    sgf = readSgfFile(results.rest[0]);
  } else {
    sgf = readSgfFromStdin();
  }

  SGFParser sgfParser = SGFParser.fromFile(sgf);
  sgfParser.parseBuffer();

  SGFTreeNode sgfTree = sgfParser.games[0];

  Orientation targetOrientation = stringToOrientation[results['targetOrientation']];
  Orientation currentOrientation = findOrientationOfPosition(sgfTree);
  int rotationSteps = targetOrientation.index - currentOrientation.index;
  if (rotationSteps < 0) {
    rotationSteps += Orientation.values.length;
  }

  rotateTree(sgfTree, rotationSteps);

  String resultingSgf = SGFWriter(application: 'sgforient:1').writeSgf(sgfTree);
  print(resultingSgf);
}
